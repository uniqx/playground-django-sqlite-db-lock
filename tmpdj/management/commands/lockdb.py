from django.core.management.base import BaseCommand, CommandError
from django.db import transaction
import time

from tmpdj.models import Person

class Command(BaseCommand):
    # uncomment this decoartor to reproduce:
    # https://gitlab.com/fdroid/repomaker/issues/164
    #
    # steps to reproduce:
    # 1. start server: python manage.py runserver
    # 2. start this command: pyhton manage.py lockdb
    # 3. open http://localhost:8000/admin/tmpdj/person
    # 4. add a new person
    # 5. db locked error
    #
    #@transaction.atomic
    def handle(self, *args, **options):
        cnt = 0
        while True:

            cnt += 1
            self.dbOp()

            pcnt = len(Person.objects.all())
            self.stdout.write("iteration {:5} complete (db contains {:2} persons) ...".format(cnt, pcnt))

            # give other processes a chance to access db file
            time.sleep(.1)

    @transaction.atomic
    def dbOp(self):
        try:
            p = Person.objects.get(first_name="Test", last_name="Dummy")
            p.delete()
        except Person.DoesNotExist:
            p = Person()
            p.first_name = "Test"
            p.last_name = "Dummy"
            p.save()
        # hold db-lock for an additional second
        time.sleep(1)
