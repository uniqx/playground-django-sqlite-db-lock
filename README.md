# illustration of a custom command with respect to an issue in repomaker

quickstart:

    git clone https://gitlab.com/uniqx/playground-django-sqlite-db-lock.git
    cd playground-django-sqlite-db-lock
    virtualenv --python python3 env
    . env/bin/activate
    pip install -r requirements.txt
    python manage.py makemigrations
    python manage.py migrate
    python manage.py createsuperuser

then run those two commands concurrently:

    # start long running background task
    python manage.py lockdb

    # start debug server
    python manage.py runserver

Now open admin interface with browser http://localhost:8000/admin/tmpdj/person and create and save a new persion to see if the database is locked.

To reproduce a database lock look into: _tmpdj/management/commands/lockdb.py_
